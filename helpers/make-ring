#!/bin/sh
#
#    Copyright (C) 2023 Luis Guzman <ark@switnet.org>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

EXTERNAL='deb-src http://ftp.debian.org/debian sid main'
REPOKEY=6ED0E7B82643E131
VERSION=1

. ./config

# Add trisquel aramo's dependencies
sed -i "/# Qt client/i # trisquel's aramo dependencies" debian/control
for i in    "libgl1-mesa-dev" \
            "libqt6opengl6-dev" \
            "libvulkan-dev" \
            "libxkbcommon-dev"
do
    sed -i "/# Qt client/i \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ $i," debian/control
done


# Add aramo's missing runtime dependencies
for i in    "libqt6multimediaquick6" \
            "libqt6quickshapes6" \
            "libqt6quicktemplates2-6" \
            "qt6-qpa-plugins"
do
    sed -i "/jami-daemon (=/i \ \ \ \ \ \ \ \ \ $i," debian/control
done

# Remove not available qt libraries at Trisquel 11.0
for i in 	qt6-5compat-dev \
		qml6-module-qtquick3d-spatialaudio
do
    sed -i "/$i/d" debian/control
done

# Removing ffmpeg time_base feature req. from jami so it builds
# against libavutil56 (ffmpeg 4.4). Can be removed on Trisquel 12
patch -R -p1 < $DATA/ffmpeg_time_base_frame.patch

# Backport latest changes required at experimental - Remove once package
# keep up with changes at testing or unstable release.
for patch in $(ls -v ${DATA}/debian_backport/*.patch)
do
echo "Applying $patch"
patch -Np1 < $patch
done

changelog "Backport ring/jammi to aramo from debian sid"

compile
