diff --git a/README.md b/README.md
index aaa7caa..293545b 100644
--- a/README.md
+++ b/README.md
@@ -1,7 +1,3 @@
-[![pipeline status](https://salsa.debian.org/freedombox-team/freedom-maker/badges/master/pipeline.svg)](https://salsa.debian.org/freedombox-team/freedom-maker/commits/master)
-[![Debian Unstable](https://badges.debian.net/badges/debian/unstable/freedom-maker/version.svg)](https://packages.debian.org/unstable/freedom-maker)
-[![Debian Testing](https://badges.debian.net/badges/debian/testing/freedom-maker/version.svg)](https://packages.debian.org/testing/freedom-maker)
-
 # Freedom-Maker: The FreedomBox image builder
 
 These scripts build FreedomBox-images for various supported hardware
@@ -13,9 +9,9 @@ during releases and for advanced users who intend to build their own
 images. Regular users who wish to turn their devices into
 FreedomBoxes should instead download the pre-built images.
 
-Get a pre-built image via https://freedombox.org/download/.  There
+Get a pre-built image via https://cdimage.trisquel.info/trisquel-images/. There
 are images available for all supported target devices.  You also find
-the setup instructions on the [Wiki](https://wiki.debian.org/FreedomBox/).
+the setup instructions on the [Wiki](https://trisquel.info/en/wiki/FreedomBox/).
 
 If you wish to create your own FreedomBox image, perhaps with some
 tweaks, see the *Build Images* section below.
@@ -24,7 +20,7 @@ tweaks, see the *Build Images* section below.
 
 ## Supported Targets
 
-Freedom-maker supports building for the following targets:
+Trisquel Freedom-maker supports building for the following targets:
 
 | target                | description |
 |-----------------------|-------------|
@@ -46,25 +42,18 @@ Freedom-maker supports building for the following targets:
 | *pine64-plus*		| Pine64+ board's SD card |
 | *qemu-amd64*		| 64-bit image for the Qemu virtualization tool |
 | *qemu-i386*		| 32-bit image for the Qemu virtualization tool |
-| *raspberry2*		| RasbperryPi 2's SD card |
-| *raspberry3*		| RasbperryPi 3's SD card |
-| *raspberry3-b-plus*	| RasbperryPi 3 Model B+'s SD card |
-| *test*		| build virtualbox i386 image and run diagnostics tests on it
-| *virtualbox-amd64*	| 64-bit image for the VirtualBox virtualization tool |
-| *virtualbox-i386*	| 32-bit image for the VirtualBox virtualization tool |
 
 ## Running Build
 
-1. Fetch the git source of freedom-maker:
+1. Fetch the 'freedom-maker' source code:
     ```
-    $ git clone https://salsa.debian.org/freedombox-team/freedom-maker.git
+    $ apt source freedom-maker
     ```
 
 2. Install the required dependencies:
     ```shell
     $ sudo apt install btrfs-progs debootstrap kpartx parted qemu-user-static qemu-utils sshpass
-    $ cd freedom-maker
-    $ sudo apt build-dep .
+    $ sudo apt build-dep freedom-maker
     ```
 
 3. Build images:
@@ -73,6 +62,7 @@ Freedom-maker supports building for the following targets:
     to run "parted".
 
     ```
+    $ cd $(find -type d -name 'freedom-maker-*')
     $ sudo python3 -m freedommaker <TARGET>
     ```
     where: &lt;TARGET&gt; is one of the 'Supported Targets' above.
diff --git a/debian/freedom-maker.1 b/debian/freedom-maker.1
index 71a9a9b..58051f3 100644
--- a/debian/freedom-maker.1
+++ b/debian/freedom-maker.1
@@ -51,17 +51,17 @@ Size of the image to build
 .PP
 \fB\-\-build\-mirror\fR
 .RS 4
-Debian mirror to use for building
+Trisquel mirror to use for building
 .RE
 .PP
 \fB\-\-mirror\fR
 .RS 4
-Debian mirror to use in built image
+Trisquel mirror to use in built image
 .RE
 .PP
 \fB\-\-distribution\fR
 .RS 4
-Debian release to use in built image
+Trisquel release to use in built image
 .RE
 .PP
 \fB\-\-package\fR
@@ -101,7 +101,7 @@ Force rebuild of images even when required image exists
 .PP
 \fBtargets\fR
 .RS 4
-Image targets to build\&. Choose one or more of a20\-olinuxino\-lime, a20\-olinuxino\-lime2, a20\-olinuxino\-micro, amd64, arm64, armhf, banana\-pro, beaglebone, cubieboard2, cubietruck, i386, lamobo\-r1, orange\-pi\-zero, pcduino3, pine64\-lts, pine64\-plus, qemu\-amd64, qemu\-i386, raspberry2, raspberry3, raspberry3\-b\-plus, test, virtualbox\-amd64, virtualbox\-i386
+Image targets to build\&. Choose one or more of freedommaker, dreamplug, beaglebone, cubieboard2, cubietruck, a20\-olinuxino\-lime, a20\-olinuxino\-lime2, a20\-olinuxino\-micro, i386, amd64, qemu\-i386, qemu\-amd64, pcDuino3
 .RE
 .SH "EXAMPLES"
 .PP
@@ -128,8 +128,7 @@ Build a FreedomBox image for the BeagleBone Single Board Computer\&.
 $ freedommaker a20\-olinuxino\-lime a20\-olinuxino\-lime2
       a20\-olinuxino\-micro amd64 arm64 armhf banana\-pro beaglebone cubieboard2
       cubietruck i386 lamobo\-r1 orange\-pi\-zero pcduino3 pine64\-lts pine64\-plus
-      qemu\-amd64 qemu\-i386 raspberry2 raspberry3 raspberry3\-b\-plus test
-      virtualbox\-amd64 virtualbox\-i386
+      qemu\-amd64 qemu\-i386
 .fi
 .if n \{\
 .RE
